// Because this serializer is forming a query string, we need to use a delimiter
// other than the "&" character so that the query can be combined with other
// queries in the URL.
const formDelimiter = "+";

// Private delimited to be used just in this utility. Should be something
// unique but this will never be saved in URL state or be visible to the user
const privateDelimiter = "instapro-delimiter";

/**
 * Serialize form data into a query string
 * @see https://htmldom.dev/serialize-form-data-into-a-query-string
 */
const serializeFormData = (formElement) => {
  // Get all fields
  const fields = [].slice.call(formElement.elements, 0);

  return fields
    .map((element) => {
      const { name, type } = element;

      // We ignore
      // - field that doesn't have a name
      // - disabled field
      // - `file` input
      // - unselected checkbox/radio
      if (
        !name ||
        element.disabled ||
        type === "file" ||
        (/(checkbox|radio)/.test(type) && !element.checked)
      ) {
        return "";
      }

      // Multiple select
      if (type === "select-multiple") {
        return Array.from(element.options)
          .map((option) => {
            return option.selected
              ? `${encodeURIComponent(name)}=${encodeURIComponent(
                  option.value
                )}`
              : "";
          })
          .filter((item) => {
            return item;
          })
          .join(formDelimiter);
      }

      return `${encodeURIComponent(name)}=${encodeURIComponent(element.value)}`;
    })
    .filter((item) => {
      return item;
    })
    .join(formDelimiter);
};

export const getServiceSlug = (serializedForm = "") => {
  const formParts = serializedForm.split(formDelimiter);
  const keyValuePair = formParts.find((part) => part.includes("serviceSlug"));

  if (keyValuePair) {
    return keyValuePair.split("=")[1];
  }

  return null;
};

export const serializeForm = (formElement, serviceSlug) => {
  const serializedForm = serializeFormData(formElement);

  if (serviceSlug) {
    return `serviceSlug=${serviceSlug}${formDelimiter}${serializedForm}`;
  }

  return serializedForm;
};

/**
 * Decode the string and escape certain special characters
 *
 * @param {String} str The string to transform
 * @returns {String} Decoded and escaped of special charaters that would
 *                    break JSON parsing
 */
const decodeAndEscapeString = (str) =>
  decodeURIComponent(decodeURIComponent(str))
    .replace(/(^\s*(?!.+)\n+)|(\n+\s*(?!.+)$)/g, "")
    .replace(/"/g, `\\$&`)
    .replace(/[\n]/gi, "\\n");

/**
 * Check if a string needs to be decoded (by checking for certian
 * characters) and if so, decode it. If not, then return the original.
 *
 * @param {String} str The string that might need to be decoded
 * @returns {String}
 */
const decodeStringIfNeeded = (str) => {
  if (str.includes("=")) {
    return str;
  }
  return decodeURIComponent(decodeURIComponent(str));
};

/**
 * Take a serialized Service Request form and convert it to a JSON object
 *
 * @param {String} serializedForm Service Request form taken from URL
 * @returns {Object} Returns the JSON representation of saved form state
 */
export const deserializeForm = (serializedForm = "") => {
  let form = {};

  const delimiter = new RegExp(
    formDelimiter.replace(/[-/\\^$*+?.()|[\]{}]/g, "\\$&"),
    "g"
  );

  // Split the string by key/value pairs and decode the values as needed
  const jsonString = decodeStringIfNeeded(serializedForm)
    .replace(delimiter, privateDelimiter)
    .replace(/=/g, ":")
    .split(privateDelimiter)
    .reduce((acc, str, index, keyValuePairs) => {
      const [rawKey, ...valuesArr] = str.split(":");
      const key = rawKey.replace(":", "");
      const value = valuesArr.join(":");

      if (key === "") {
        // If the key is empty, it is probably that the form
        // is really not valid. Return an early end to the object
        // so that we can return early.
        return `${acc}}`;
      }

      const ending = index + 1 === keyValuePairs.length ? "}" : ",";

      return `${acc}"${decodeAndEscapeString(key)}":"${decodeAndEscapeString(
        value
      )}"${ending}`;
    }, "{")
    .replace(new RegExp(privateDelimiter, "g"), ",");

  try {
    form = JSON.parse(jsonString);
  } catch (error) {
    // istanbul ignore next
    // If I knew what would make the JSON.parse throw, I would have
    // written a better deserializer 😓 But for now I need these
    // errors to be reported so that the deserializer can be made
    // complete. But I can't write a test that makes these lines run.
    import("utils/report").then(async ({ default: report }) => {
      report({
        error,
        errorClass: "URL Restoration fail",
        errorContext: "Service Request Flow",
        values: serializedForm,
      });
    });
  }

  return form;
};
